import React, { Component } from 'react';
import './App.css';

import { Container, Col, Row } from 'react-grid-system';
import HeaderLogo from './containers/HeaderLogo';
import TicketsList from './containers/Tickets';
import Filter from './containers/Filter';

class App extends Component {
  state = {
    filter: {
      stops_count: ['All'],
      currency: 1
    }
  };

  onFilterChange = (filter) => {
    if (filter.stops_count.length === 0) {
      filter.stops_count = ['All'];
    }

    this.setState({ filter });
  };

  render() {
    return (
      <Container className="App">
        <Row>
          <Col xs={12}>
            <HeaderLogo />
          </Col>
        </Row>

        <Row style={{marginTop: '50px'}}>
          <Col xs={1}/>
          <Col className='filter-col' xs={12} xl={3}>
            <Filter actualFilter={this.state.filter} onFilterChange={this.onFilterChange}/>
          </Col>

          <Col className='tickets-col' xs={12} xl={7}>
            <TicketsList filter={this.state.filter}/>
          </Col>
          <Col xs={1}/>
        </Row>
      </Container>
    );
  }
}

export default App;
