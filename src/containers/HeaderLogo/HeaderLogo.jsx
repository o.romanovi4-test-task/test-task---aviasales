import React, { Component } from 'react';
import './HeaderLogo.css';

import Logo from '../../img/Logo.png';

class HeaderLogo extends Component {
    render() {
        return (
            <div className='header_logo'>
                <img src={Logo} alt="aviasales-logo"/>
            </div>
        );
    }
}

export default HeaderLogo;