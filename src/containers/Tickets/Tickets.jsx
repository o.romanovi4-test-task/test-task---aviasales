import React, { Component } from 'react';
import Ticket from '../../components/Ticket';
import './Tickets.css';

class Tickets extends Component {
    state = {
        tickets: [],
        apiLoaded: false
    };

    sortByPrice(tickets) {
        let ticketsSorted = tickets.slice();
        ticketsSorted.sort((a, b) => {
            if (a.price > b.price) return 1;
            if (a.price < b.price) return -1;
            return 0;
        });

        return ticketsSorted;
    }

    componentDidMount() {
        // Async get the tickets
        fetch(`${process.env.PUBLIC_URL}/tickets/tickets.json`)
            .then(res => res.json())
            .then(jsonData => {
                let tickets = jsonData.tickets;
                this.setState({ tickets });
            })
            .catch(err => {
                console.error(err);
                this.setState({tickets: null});
            })
            .finally(() => this.setState({apiLoaded: true}));
    }

    filterTickets = () => {
        const filterByStopsCount = (stopsCount, tickets) => {
            let filteredTickets = [];
            for (let i = 0; i < stopsCount.length; i++) {
                if (stopsCount[i] === 'All') {
                    filteredTickets = tickets;
                    break;
                } else {        
                    filteredTickets = filteredTickets.concat(tickets.filter(ticket => ticket.stops === stopsCount[i]))
                }
            }
    
            return filteredTickets;
        };

        const currencyConverter = (currency, tickets) => {
            return tickets.map(ticket => {
                return {
                    ...ticket,
                    price: (ticket.price / currency).toFixed(2)
                }
            })
        };

        const { stops_count, currency } = this.props.filter;
        const { tickets } = this.state;

        let filteredTickets = filterByStopsCount(stops_count, tickets);
        console.log(stops_count, currency);
        filteredTickets = currencyConverter(currency, filteredTickets);

        return filteredTickets;
    }

    printTickets() {
        if (!this.state.apiLoaded) {
            return (<h4>Загрузка...</h4>);
        }

        const { tickets } = this.state;
        if (tickets && Array.isArray(tickets) && tickets.length > 0) {
            return this.sortByPrice(this.filterTickets()).map((ticket, index) => (
                <Ticket key={index} {...ticket}/>
            ));
        }

        else {
            return (<h4>Ошибка загрузки!</h4>);
        }
    }

    render() {
        return (
            <div className='tickets'>
                {this.printTickets()}
            </div>
        );
    }
}

export default Tickets;