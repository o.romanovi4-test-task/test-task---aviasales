import React, { Component } from 'react';
import './Filter.css';
import Card from '../../components/Card';
import Title from '../../components/Title';

import CheckBoxGroup from '../../components/CheckBoxGroup';
import RadioButtonGroup from '../../components/RadioButtonGroup';

const stopsFilter = require('./options/count-stops');
const currencyFilter = require('./options/currency');

class Filter extends Component {
    render() {
        return (
            <Card className='filter'>
                <div className="filter__row">
                    <Title>Валюта</Title>
                    <RadioButtonGroup 
                        actualFilter={this.props.actualFilter} 
                        onFilterChange={this.props.onFilterChange} 
                        options={currencyFilter} 
                        groupName='currency'
                    />
                </div>

                <div className="filter__row filter__count-stops">
                    <Title>Количество пересадок</Title>
                    <CheckBoxGroup 
                        actualFilter={this.props.actualFilter} 
                        onFilterChange={this.props.onFilterChange} 
                        options={stopsFilter}
                    />
                </div>
            </Card>
        );
    }
}

export default Filter;