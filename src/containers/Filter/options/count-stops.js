const stopsFilter = [
    { title: 'Все', value: 'All', selectAll: true },
    { title: 'Без пересадок', value: 0, canOnlySelected: true },
    { title: '1 пересадка', value: 1, canOnlySelected: true },
    { title: '2 пересадки', value: 2, canOnlySelected: true },
    { title: '3 пересадки', value: 3, canOnlySelected: true },
];

module.exports = stopsFilter;