const currency = [
    { title: 'RUB', value: 1, selected: true },
    { title: 'USD', value: 68 },
    { title: 'EUR', value: 78 }
];

module.exports = currency;