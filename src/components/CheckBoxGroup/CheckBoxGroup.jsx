import React, { Component } from 'react';
import CheckBox from '../CheckBox';

class CheckBoxGroup extends Component {
    state = {
        options: this.props.options
    };

    selectAllHandle = () => {
        this.setState({options: this.state.options.map(option => {
            return {
                ...option,
                selected: true
            }
        })}, () => {
            this.filterUpdate();
        })
    };

    resetSelected = () => {
        return this.state.options.map(option => {
            return {
                ...option,
                selected: false
            }
        })
    };

    selectOnlyOneHandle = (selectIndex = 0) => {
        const options = this.resetSelected();
        options[selectIndex].selected = true;
        this.setState({ options }, () => {
            this.filterUpdate();
        });
    };

    onSelectHandle = (status, newSelectIndex) => {  
        const { options } = this.state;
        options[newSelectIndex].selected = status;

        // Обрабатываем случай если в группе есть selectAll объект
        // Если есть какой-то чекбокс с НЕ selected, то убираем selected у selecAll
        const selectAllOptionIndex = options.findIndex(option => option.selectAll);
        if (selectAllOptionIndex !== -1) {
            const selectAllOptionsLength = options.filter(option => option.selectAll).length;
            const notSelected = options.filter(option => !option.selected && !option.selectAll);
            if (notSelected.length > 0) {
                options[selectAllOptionIndex].selected = false;
            }

            // Если выбраны все чекбоксы, то ставим select в true для selectAll чекбокса
            const isSelectedAll = options.filter(option => option.selected && !option.selectAll).length === options.length - selectAllOptionsLength;
            if (isSelectedAll) {
                this.selectAllHandle();
                return;
            }
        }

        this.setState({ options }, () => {
            this.filterUpdate();
        });
    };

    updateCheckboxes() {
        return this.state.options.map((box, index) => (
            <CheckBox 
                {...box}
                key={index}
                index={index}
                selectOnlyHandle={() => { this.selectOnlyOneHandle(index); }} 
                selectHandle={this.onSelectHandle}
                selectAllHandle={box.selectAll ? () => { this.selectAllHandle(); } : null}
            />
        ));
    }

    filterUpdate = () => {
        if (this.props.onFilterChange) {
            this.props.onFilterChange({
                currency: this.props.actualFilter.currency,
                stops_count: this.state.options.filter(option => option.selected).map(option => option.value)
            });
        }
    }

    render() {
        return (
            <form className='checkbox-group'>
                {this.updateCheckboxes()}
            </form>
        );
    }
}

export default CheckBoxGroup;