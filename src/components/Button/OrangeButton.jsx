import React, { Component } from 'react';
import Button from './Button';
import './styles/orange-button.css';

class OrangeButton extends Component {
    render() {
        return (
            <Button className='button__orange' {...this.props}>
                {this.props.children}
            </Button>
        );
    }
}

export default OrangeButton;