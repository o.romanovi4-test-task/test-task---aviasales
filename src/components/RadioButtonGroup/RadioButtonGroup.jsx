import React, { Component } from 'react';
import './RadioButtonGroup.css';

import RadioButton from '../RadioButton';

class RadioButtonGroup extends Component {
    state = {
        options: this.props.options
    }

    resetAllSelect = () => {
        return this.state.options.map(option => {
            return {
                ...option,
                selected: false
            }
        });
    };

    onSelectHandle = (indexSelected) => {
        const options = this.resetAllSelect();
        options[indexSelected].selected = true;
        this.setState({ options }, () => {
            this.filterUpdate();
        });
    };

    updateRadiobuttons = () => {
        return this.state.options.map((option, index) => (
            <RadioButton 
                {...option} 
                key={index}
                name={this.props.groupName || ''}
                onSelectHandle={() => { this.onSelectHandle(index) }}
            />
        ))
    };

    filterUpdate = () => {
        if (this.props.onFilterChange) {
            this.props.onFilterChange({
                currency: this.state.options.find(option => option.selected).value,
                stops_count: this.props.actualFilter.stops_count
            });
        }
    };

    render() {
        return (
            <form className='radiobutton-group'>
                {this.updateRadiobuttons()}
            </form>
        );
    }
}

export default RadioButtonGroup;