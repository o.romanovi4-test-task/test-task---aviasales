import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Ticket.css';

import Card from '../../components/Card';
import { OrangeButton } from '../../components/Button';
import { Container, Row, Col } from 'react-grid-system';

import AirlineLogo from '../../img/airlines-logo/turkish-airlines.png';
import { OriginInfo, DestinationInfo } from './PlaceDateTimeInfo';
import StopsInfo from './Stops';

class Ticket extends Component {
    collectDestinationProps() {
        return {
            time: this.props.arrival_time,
            place: `${this.props.destination}, ${this.props.destination_name}`,
            date: this.props.arrival_date
        }
    }

    collectOriginProps() {
        return {
            time: this.props.departure_time,
            place: `${this.props.origin}, ${this.props.origin_name}`,
            date: this.props.departure_date
        }
    }

    render() {
        return (
            <Card className='ticket'>
                <Container>
                    <Row>
                        <Col xs={12} md={4}>
                            <img className='ticket__airline-logo' src={AirlineLogo} alt="turkish-airline"/>
                            <OrangeButton>Купить за {this.props.price}</OrangeButton>
                        </Col>

                        <Col className='ticket-col-info' xs={12} md={8} style={{display: 'flex', justifyContent: 'space-between'}}>
                            <OriginInfo {...this.collectOriginProps()}/>
                            <StopsInfo count={this.props.stops}/>
                            <DestinationInfo {...this.collectDestinationProps()}/>
                        </Col>
                    </Row>
                </Container>
            </Card>
        );
    }
}

Ticket.propTypes = {
    price: PropTypes.string.isRequired
}

export default Ticket;