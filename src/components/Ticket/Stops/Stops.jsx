import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getPluralEnding } from '../../../lib/plural';
import './Stops.css';

import PlaneIcon from '../../../img/Shapes/Plane.png';

class Stops extends Component {
    render() {
        const { count } = this.props;

        return (
            <div className='ticket__stops stops'>
                <div className="stops__count">{count} {getPluralEnding(count, 'пересадка', 'пересадки', 'пересадок').toUpperCase()}</div>

                <div className="stops__shape">
                    <hr color='#D2D5D6'/>
                    <img src={PlaneIcon} alt="plane-icon"/>
                </div>
            </div>
        );
    }
}

Stops.propTypes = {
    count: PropTypes.number.isRequired
};

export default Stops;