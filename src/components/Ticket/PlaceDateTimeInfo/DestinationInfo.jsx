import React, { Component } from 'react';
import PlaceDateTime from './PlaceDateTimeInfo';

class DestinationInfo extends Component {
    render() {
        return (
            <PlaceDateTime direction='destination' {...this.props}></PlaceDateTime>
        );
    }
}

export default DestinationInfo;