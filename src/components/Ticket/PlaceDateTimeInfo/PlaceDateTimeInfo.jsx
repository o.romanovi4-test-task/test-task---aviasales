import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './PlaceDateTimeInfo.css';

class PlaceDateTimeInfo extends Component {
    /**
     * Выводим дату, преобразуя в нужный формат
     */
    printDate() {
        // Сплитуем строку по . символу, так как new Date() не совсем корректно может спарсить dd.mm.yyyy
        let date = this.props.date.split('.');
        
        // Так как год в дате билета указана 18, то необходимо как-то указать Date точную дату]
        // Ибо если просто прокинуть 18, то получится 1918, что нам не нужно
        // Но желательно иметь дату по стандарту ISO
        date = new Date(+`20${date[2]}`, date[1], date[0]);

        const shortDate = date.toLocaleString('ru-RU', {day: 'numeric', month: 'short'}).replace('.', '');
        const shortWeekday = date.toLocaleString('ru-RU', { weekday: 'short' });
        return `${shortDate} ${date.getFullYear()}, ${shortWeekday[0].toUpperCase() + shortWeekday.slice(1)}`;
    }

    render() {
        return (
            <div className={`ticket__place-info place-info place-info--${this.props.direction}`}>
                <div className="place-info__time">{this.props.time}</div>
                <div className="place-info__name">{this.props.place}</div>
                <div className="place-info__date">{this.printDate()}</div>
            </div>
        );
    }
}

PlaceDateTimeInfo.propTypes = {
    time: PropTypes.string.isRequired,
    place: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    direction: PropTypes.string.isRequired
};

export default PlaceDateTimeInfo;