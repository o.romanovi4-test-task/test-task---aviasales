import React, { Component } from 'react';
import PlaceDateTime from './PlaceDateTimeInfo';

class OriginInfo extends Component {
    render() {
        return (
            <PlaceDateTime direction='origin' {...this.props}></PlaceDateTime>
        );
    }
}

export default OriginInfo;