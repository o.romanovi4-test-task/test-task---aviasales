import OriginInfo from './OriginInfo';
import DestinationInfo from './DestinationInfo';

export { OriginInfo, DestinationInfo };