import React, { Component } from 'react';
import './RadioButton.css';

class RadioButton extends Component {
    render() {
        return (
            <label className="radiobutton">
                <input onChange={this.props.onSelectHandle} type="radio" checked={this.props.selected}/>
                <span className="radiobutton__title">{this.props.title}</span>
                <span className="checkmark"></span>
            </label>
        );
    }
}

export default RadioButton;