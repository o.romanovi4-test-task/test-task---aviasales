import React, { Component } from 'react';
import './Checkbox.css';

class Checkbox extends Component {
    state = {
        onlySelectedStatusUpdated: false
    };

    printCanSelectedOnly() {
        if (this.props.canOnlySelected) {
            return (
                <div
                    onClick={() => {
                        // Обновляем состояние для того, чтобы контролировать onChange
                        this.setState({onlySelectedStatusUpdated: true}, () => {
                            this.props.selectOnlyHandle(); 
                        });
                    }} 
                    className="checkbox__select-only">
                    Только
                </div>
            );
        }
    }

    onChange = (e) => {
        if (this.props.selectAllHandle) {
            this.props.selectAllHandle();
        }

        else {
            // Не вызываем обработчик, если предыдущий шаг был select только одного checkbox'а
            if (this.state.onlySelectedStatusUpdated) {
                this.setState({onlySelectedStatusUpdated: false});
                return;
            }

            this.props.selectHandle(e.target.checked, this.props.index);
        }
    }

    render() {
        return (
            <label className="checkbox">
                {this.props.title}
                <input onChange={this.onChange} type="checkbox" checked={this.props.selected || false} />
                <span className="checkmark"></span>

                {this.printCanSelectedOnly()}
            </label>
        );
    }
}

export default Checkbox;