import React, { Component } from 'react';
import './Card.css';

class Card extends Component {
    render() {
        return (
            <div {...this.props} className={`card ${this.props.className}`}>
                {this.props.children}
            </div>
        );
    }
}

export default Card;