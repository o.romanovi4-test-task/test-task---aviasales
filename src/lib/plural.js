const getPluralEnding = (count, form1, form2, form3) => {
    const temp100 = count % 100;
    const temp10 = count % 10;
    
    if (temp100 > 10 && temp100 < 20) return form3;
    if (temp10 > 1 && temp10 < 5) return form2;
    if (temp10 === 1) return form1;

    return form3;
};

export { getPluralEnding };